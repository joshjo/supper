from __future__ import unicode_literals

from django.db import models

class HouseType(models.Model):
    name = models.CharField(max_length=128)


class House(models.Model):
    name = models.CharField(max_length=128)
    amount = models.DecimalField(decimal_places=2, max_digits=7)
    house_type = models.ForeignKey(HouseType)
    created = models.DateField()
  
