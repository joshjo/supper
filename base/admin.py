from django.contrib import admin
from models import *

admin.site.register(HouseType)
admin.site.register(House)

# Register your models here.
